#include <stdio.h>
#include <string.h>

struct autos
{
  int año;
  float kilometraje;
  char marca[10];
  char modelo[10];
  char motor[10];
  char color[6];
};

struct computadora
{
  int memoria;
  int procesador;
  char marca[15];
  char color[15];
  char generacion[10];
};

struct alumno
{
  int edad;
  float nacionalidad;
  char nombre[25];
  char noCuenta[15];
  char carrea[20];
  char Facultad[25];
};

struct equipodefutbol
{
  int cantidad de jugadores;
  char nacionalidad[15];
  int no.jugadores;
  int posicion;
};

struct Mascota
{
  int edad;
  float peso;
  char nombre[15];
  char raza[15];
  char color[10];
};

struct VideoJuego
{
  int costo;
  char Titulo[15];
  char Categoria[15];
  char Consola[10];
};

struct SuperHeroe
{
  int edad;
  float origen;
  char Nombre[15];
  char Poderes[150];
  char bando[15];
  char Traje[20];
};

struct Planetas
{
  int tiempo;
  float dimensiones;
  int distancia;
  char nombre[15];
  char galaxia[15];
};

struct Messi
{
  int edad;
  float estatura;
  int goles[30];
  char nombre[15];
  char equipo[20];
  char nacionalidad[25];
  int campeonatos;

};

struct Punisherkillsmarvel
{
   int lanzamiento[15];
   char personajes[35];
   char copias[25];
   int autor;
}
